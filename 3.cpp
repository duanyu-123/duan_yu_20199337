#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
void menu(); 
void help();
void one();void operation_1(); 
void two();void operation_2();
void three();void operation_3(); 
void error();
int main()
{
int opt=1,n;
printf("==========口算生成器==========\n");
printf("欢迎使用口算生成器:\n");
printf("\n");
help();
while(opt!=0)
{
menu();
printf("请输入操作> ");
scanf("%d",&opt);
printf("<执行操作:)\n");
printf("\n");
switch(opt)
{
case 1:operation_1();break;
case 2:operation_2();break;
case 3:operation_3();break;
case 4:help();break;
case 5:printf("程序结束, 欢迎下次使用\n");
printf("任意键结束……");
opt=0;
default:error();break;
}
}
return 0;
}
void menu()
{
printf("\n");
printf("操作列表:\n");
printf("1)一年级 2)二年级 3)三年级\n");
printf("4)帮助 5)退出程序\n");
}
void help()
{
printf("\n");
printf("帮助信息\n");
printf("您需要输入命令代号来进行操作,且\n");
printf("一年级题目为不超过十位的加减法\n");
printf("二年级题目为不超过百位的乘除法\n");
printf("三年级题目为不超过百位的加减乘除混合题目.\n");
}
void operation_1() 
{
printf("请输入生成个数>");
one();
}
void operation_2()
{
printf("请输入生成个数>");
two();
}
void operation_3()
{
printf("请输入生成个数>");
three();
}
void one()
{
int n,a,b,c,d;
time_t t;
srand((unsigned) time(&t));
printf("现在是一年级题目:\n");
scanf("%d",&n);
printf("<执行操作:)\n");
for (int i=0;i<n;i++)
{
    a=rand() % 10;
    b=rand() % 10;
    c=rand() % 2;
    if (c==0)
    {
    
       d=a+b;
       printf("%d + %d = %d\n",a,b,d);
    }
    else
    {
       d=a-b;
       printf("%d - %d = %d\n",a,b,d);
       }
}

}
void two()
{
int n,a,b,c;
float d;
time_t t;
srand((unsigned) time(&t));
printf("现在是二年级题目:\n");
scanf("%d",&n);
printf("<执行操作:)\n");
for (int i=0;i<n;i++)
{
    a=rand() % 10;
    b=rand() % 10;
    c=rand() % 2;
    if (c==0)
    {
       d=a*b;
       printf("%d * %d = %g\n",a,b,d);
       }
    else{
    while(b==0)
    {
        b=rand() % 10;
     } 
       d=a/(b*1.0);
       printf("%d / %d = %g\n",a,b,d);
       }
}

}
void three()
{
 int n,m,i,a,b,c;
float z;
char fh1[2],fh2[2];
time_t t;
srand((unsigned) time(&t));
printf("现在是三年级题目:\n");
scanf("%d",&n);
printf("<执行操作:)\n");
char fh[4][6] = {"*","/","+","-"};
 for (int i=0; i<n; i++)
{
    a=rand() % 100;
    b=rand() % 100;
    c=rand() % 100;
    while (a==0||b==0||c==0)
    {
        a=rand() % 100;b=rand() % 100;c=rand() % 100; 
    }
    strcpy(fh1,fh[rand() % 3]);
    strcpy(fh2,fh[rand() % 3]);
    if(strcmp(fh1,"*")==0&&strcmp(fh2,"*")==0)
    {
        z = a * b * c;
    printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"*")==0&&strcmp(fh2,"/")==0)
    {
        z = a * b / (c*1.0);
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"*")==0&&strcmp(fh2,"+")==0)
    {
        z = a * b + c;
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"*")==0&&strcmp(fh2,"-")==0)
    {
        z = a * b - c;
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"/")==0&&strcmp(fh2,"+")==0)
    {
        z = a / (b*1.0) + c;
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"/")==0&&strcmp(fh2,"-")==0)
    {
        z = a / (b*1.0) - c;
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"/")==0&&strcmp(fh2,"/")==0)
    {
        z = a / (b*1.0) / (c*1.0);
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"+")==0&&strcmp(fh2,"-")==0)
    {
        z = a + b - c;
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"+")==0&&strcmp(fh2,"+")==0)
    {
        z = a + b + c;
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"-")==0&&strcmp(fh2,"-")==0)
    {
        z = a - b - c;
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"/")==0&&strcmp(fh2,"*")==0)
    {
        z = a / (b*1.0) * c;
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"+")==0&&strcmp(fh2,"/")==0)
    {
        z = a + b / (c*1.0);
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"+")==0&&strcmp(fh2,"*")==0)
    {
        z = a + b * c;
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"-")==0&&strcmp(fh2,"/")==0)
    {
        z = a - b / (c*1.0);
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"-")==0&&strcmp(fh2,"+")==0)
    {
        z = a - b + c;
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
    else if(strcmp(fh1,"-")==0&&strcmp(fh2,"*")==0)
    {
        z = a - b * c;
        printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
    }
}

}
void error()
{
printf("Error!!!\n");
printf("错误操作指令, 请重新输入\n");
}
